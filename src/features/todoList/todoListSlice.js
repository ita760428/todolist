import { createSlice } from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
  name: 'todoList',
  initialState: {
    todoList: [],
  },
  reducers: {
    getInitTodoList: (state, action) => {
      console.log(action.payload)
      state.todoList = action.payload
    },
  },
})

// Action creators are generated for each case reducer function
export const { getInitTodoList } = todoListSlice.actions

export default todoListSlice.reducer