### Objective
1.今天进行了code review，  
2.学习了react router，
3.学习了前后端交互
4.学习了promise,axios的相关知识
5.学习了抽取hook方法
6.学习了使用Ant Design


### Reflective
difficult

### Interpretive
在今天的学习中由于不熟悉Ant Design，所以在做页面美化的时候花费了很多时间，过程相当坎坷。

### Decisional
需要加强对Ant Design的使用。

