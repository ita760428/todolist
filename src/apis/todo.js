import api from './TodoApi'

export const getTododList = await api.get('/todos')

export const addTodo = (text) => {
    return api.post('/todos', {
        text,
        done: false
    })
}

export const deleteTodo = (id) => {
    return api.delete(`/todos/${id}`)
}

export const getAllTodo = async () => {
    return await api.get('/todos')
}

export const getTodoById = (id) => {
    return api.get(`/todos/${id}`)
}
export const updateTodoDone = async (id) => {
    const { data } = await getTodoById(id)

    return api.put(`/todos/${id}`, {
        done: !data.done
    })
}

export const updateTodoText = async (id, text) => {
    return api.put(`/todos/${id}`, {
        text
    })
}



