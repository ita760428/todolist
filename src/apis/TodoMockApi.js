import axios from "axios";

const mockApi = axios.create({
    baseURL: 'https://64c0b6850d8e251fd11265db.mockapi.io/api/',
})

export default mockApi