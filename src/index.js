import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import store from './app/store'
import { Provider } from 'react-redux'
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import HelpInfo from './pages/HelpInfo';
import NotFound from './pages/NotFound';
import Detail from './pages/DoneDetail/index';
import ListModal from './components/BaseModal/ListModal';
import DoneListModal from './components/BaseModal/DoneListModal';




const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([
  {
    path: '/',
    element: <App></App>,
    children: [
      {
        index: true,
        // element:<TodoList></TodoList>
        element: <ListModal></ListModal>
      },
      {
        path: '/help',
        element: <HelpInfo></HelpInfo>
      },
      {
        path: '/doneList',
        element: <DoneListModal></DoneListModal>
      },
      {
        path: '/done/:id',
        element: <Detail></Detail>
      }
    ]
  },

  {
    path: '*',
    element: <NotFound></NotFound>
  }
])


root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}></RouterProvider>
    </Provider>
  </React.StrictMode>
);

