import { useDispatch } from "react-redux"
import { addTodo, deleteTodo, updateTodoDone, updateTodoText, getAllTodo } from "../apis/todo"
import { getInitTodoList } from "../features/todoList/todoListSlice"

const useTodo = () => {

    const despatch = useDispatch()
    const getTodoList = async () => {
        const { data } = await getAllTodo()

        despatch(getInitTodoList(data))

    }
    const insertTodo = async (text) => {
        await addTodo(text)
        getTodoList()
    }
    const deleteTodoItem = async (id) => {
        await deleteTodo(id)
        getTodoList()
    }
    const updateDone = async (id) => {
        await updateTodoDone(id)
        getTodoList()
    }
    const updateText = async (id, text) => {
        await updateTodoText(id, text)
        getTodoList()
    }
    return {
        getTodoList,
        insertTodo,
        deleteTodoItem,
        updateDone,
        updateText
    }
}

export default useTodo