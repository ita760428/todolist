import { useSelector } from "react-redux";
import TodoItem from "../TodoItem";

const TodoGroup = ()=> {
    const todoList = useSelector((state) => state.todoList.todoList)

    return (
        <div>
            {todoList.map( item => (<TodoItem text={item.text} key={item.id} done={item.done} id={item.id} ></TodoItem>))}
        </div>
    )
}

export default TodoGroup;