import img from '../../components/BaseModal/img/0-11.png'
import useTodo from '../../hooks/useTodo'

const TodoItem = (props) => {

    const todo = useTodo()

    const handleChangeDone = () => {
        todo.updateDone(props.id)
    }

    const doDeleteItem = () => {
        todo.deleteTodoItem(props.id)
    }

    return (
        <div>
            <p ><span className={props.done ? 'delete' : ''} onClick={handleChangeDone}>{props.text}</span><span onClick={doDeleteItem} className='delIcon'><img src={img} /></span></p>
        </div>
    )
}

export default TodoItem;