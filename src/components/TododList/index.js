import { useDispatch } from "react-redux"
import TodoGenerator from "../TodoGenerator"
import TodoGroup from "../TodoGroup"
import { useEffect } from "react"
import useTodo from "../../hooks/useTodo"


const TodoList=()=>{

    const todo =useTodo()
    const despatch = useDispatch()
    

    useEffect(()=>{
        todo.getTodoList()
    },[])

    return(
        <div>
            <h3>Todo List</h3>
            <TodoGroup></TodoGroup>
            <TodoGenerator></TodoGenerator>
        </div>
    )
}

export default TodoList;