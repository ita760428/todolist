import React, { useState } from 'react';
import { Link } from "react-router-dom"
import { Menu } from 'antd';
const items = [
  {
    label: (
      <Link to='/'>todoList</Link>
    ),
    key: 'todoList',
  },
  {
    label: (
      <Link to="/help" >helpPage</Link>
    ),
    key: 'doneList',
  },
  {
    label: (
      <Link to="/doneList" >doneList</Link>
    ),
    key: 'help',
  },
];
const NavModal = () => {
  const [current, setCurrent] = useState('mail');
  const onClick = (e) => {
    console.log('click ', e);
    setCurrent(e.key);
  };
  return <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />;
};
export default NavModal;