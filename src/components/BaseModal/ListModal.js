import './ListModal.css'
import { Input, List, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import editIcon from './img/0-11.png'
import delIcon from './img/0-12.png'
import useTodo from '../../hooks/useTodo';
import { useSelector } from 'react-redux';
import { Button, Modal } from 'antd';


const ListModal = () => {

    const data = useSelector((state) => state.todoList.todoList)
    const todo = useTodo()
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [updateText, setUpdateText] = useState('')
    const [todoId, setTodoId] = useState('')
    const [inputText, setInputText] = useState('')

    const generateListItemSequence = async () => {
        if (inputText != '') {
            todo.insertTodo(inputText)
            setInputText('')
        }
    }
    const handleChangeText = (e) => {
        setInputText(e.target.value.trim())
    }


    useEffect(() => {
        todo.getTodoList()
    }, [])


    return (
        <>
            <List

                itemLayout="horizontal"
                dataSource={data}
                renderItem={(item, index) => {

                    const handleChangeDone = () => {
                        todo.updateDone(item.id)
                    }
                    const doDeleteItem = () => {
                        todo.deleteTodoItem(item.id)
                    }


                    const showModal = (id) => {
                        setTodoId(id)
                        setUpdateText(item.text)
                        setIsModalOpen(true);
                    }
                    const handleOk = () => {
                        console.log(todoId)
                        todo.updateText(todoId, updateText)
                        setUpdateText('')
                        setIsModalOpen(false);
                    }
                    const handleCancel = () => {
                        setUpdateText('')
                        setIsModalOpen(false);
                    }
                    const handleChangeText = (e) => {
                        setUpdateText(e.target.value.trim())
                    }

                    return (<>
                        <List.Item>
                            <List.Item.Meta
                                description={
                                    <p >
                                        <span className={item.done ? 'delete' : ''} onClick={handleChangeDone}>{item.text}</span>
                                        <span>
                                            <img src={editIcon} onClick={() => { showModal(item.id) }} />
                                            <img src={delIcon} onClick={doDeleteItem} />
                                        </span>
                                    </p>}

                            />
                        </List.Item>
                        <Modal title="edit todo" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} mask={false}>
                            <Input onChange={handleChangeText} value={updateText}></Input>
                        </Modal>

                    </>

                    )
                }}
            />



            <Space>
                <Input onChange={handleChangeText} value={inputText}></Input>
                <Button type="primary" onClick={generateListItemSequence}>add</Button>
            </Space>

        </>

    )
};
export default ListModal;