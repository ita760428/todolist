import { List } from 'antd';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import useTodo from '../../hooks/useTodo';
import { useEffect } from 'react';

const DoneListModal = () => {
    const data = useSelector((state) => state.todoList.todoList).filter(listItem => listItem.done)
    const navigate = useNavigate()
    const todo = useTodo()
    useEffect(() => {
        todo.getTodoList()
    }, [])

    return (
        <>
            <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={(item) => {

                    const handleGoToDetail = (id) => {
                        navigate(`/done/${id}`)
                    }
                    return (<>
                        <List.Item>
                            <List.Item.Meta
                                description={
                                    <p key={item.id}>
                                        <span onClick={() => { handleGoToDetail(item.id) }}>{item.text}</span>
                                    </p>}
                            />
                        </List.Item>

                    </>

                    )
                }}
            />

        </>

    )
}

export default DoneListModal