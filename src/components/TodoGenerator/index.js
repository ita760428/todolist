import { useState } from "react"
import useTodo from '../../hooks/useTodo';

const TodoGenerator = ()=>{

    const todo = useTodo()
    const [inputText,setInputText]= useState('')

    const generateListItemSequence =async()=> {
        if(inputText!=''){
            todo.insertTodo(inputText)
            setInputText('')
        }
    }

    const handleChangeText=(e)=>{
        setInputText(e.target.value.trim())
    }
    return (
        <div>
            <input type="text" onChange={handleChangeText} value={inputText}></input>
            <button onClick={generateListItemSequence}>add</button>
        </div>
    )
}  

export default TodoGenerator;