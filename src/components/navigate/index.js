import './index.css'
import { Link } from "react-router-dom"

const Navigate = ()=> {
    return (
        <div>
            <ul>
                <li>
                    <Link to='/'>todoList</Link>
                </li>
                <li>
                    <Link to="/help" >helpPage</Link>
                </li>
                <li>
                    <Link to="/doneList" >doneList</Link>
                </li>
            </ul>
        </div>
    )
}

export default Navigate