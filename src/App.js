import { Outlet } from 'react-router-dom';
import './App.css';
import NavModal from './components/BaseModal/NavModal';

function App() {
  return (
    <div className="App">
      <NavModal></NavModal>
      <Outlet></Outlet>
    </div>
  );
}

export default App;
