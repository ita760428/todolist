import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import useTodo from "../hooks/useTodo"
import { useEffect } from "react"


const DoneList = ()=> {
    const todo =useTodo()
    useEffect(()=>{
        todo.getTodoList()
    },[])
    const doneList = useSelector((state) => state.todoList.todoList).filter(listItem => listItem.done)
    const navigate = useNavigate()

    console.log(doneList)
    const handleGoToDetail = (id)=> {
        navigate(`/done/${id}`)
    }
    

    return (
        <div>
            {
                doneList.map( doneItem => <p key={doneItem.id} onClick={()=> handleGoToDetail(doneItem.id)}>{ doneItem.text}</p>)
            }
        </div>
    )
}


export default DoneList