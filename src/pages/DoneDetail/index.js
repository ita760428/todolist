import { useSelector } from "react-redux"
import { useParams } from "react-router-dom"

const Detail = ()=> {
    const {id }= useParams()
    const doneItem = useSelector((state) => state.todoList.todoList).find(item => {
        return item.id == id
    })

    return (
        <div>
            <div>{doneItem.id}</div>
            <div>{doneItem.text}</div>
        </div>
    )
}

export default Detail